package com.example.roomapp.action;

import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.example.roomapp.Constants;
import com.example.roomapp.R;
import com.example.roomapp.note.Note;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

public class NoteActionActivity extends AppCompatActivity implements NoteActionPresenter.View {
  //@BindView(R.id.activityNoteDetailsMainTitle) TextView tvMainTitle;
  @BindView(R.id.activityNoteActionTlTitle) TextInputLayout tlNoteTitle;
  @BindView(R.id.activityNoteActionTlBody) TextInputLayout tlNoteBody;
  private NoteActionPresenter noteActionPresenter;
  private boolean isEdit = false;
  private Note note;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_note_action);
    ButterKnife.bind(this);
    noteActionPresenter = new NoteActionPresenter(this);
    noteActionPresenter.setView(this);

    if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.IS_EDIT)) {
      isEdit = true;
      //tvMainTitle.setText("Editin note");
      if(getIntent().getExtras().containsKey(Constants.NOTE) && !TextUtils.isEmpty(getIntent().getExtras().getString(Constants.NOTE))) {
        note = new Gson().fromJson(getIntent().getExtras().getString(Constants.NOTE), Note.class);
        tlNoteTitle.getEditText().setText(note.getTitle());
        tlNoteBody.getEditText().setText(note.getBody());
      }
    } else {
      isEdit = false;
      //tvMainTitle.setText("Insert new note");
    }
  }

  @OnClick(R.id.activityNoteActionBtnSave)
  public void handleBtnSave() {
    if(isEdit) {
      note.setTitle(tlNoteTitle.getEditText().getText().toString());
      note.setBody(tlNoteBody.getEditText().getText().toString());
      noteActionPresenter.updateNote(note);
    } else {
      noteActionPresenter.saveNote(tlNoteTitle.getEditText().getText().toString(), tlNoteBody.getEditText().getText().toString());
    }
  }

  @Override public void onNoteAdded() {
    Toast.makeText(this, "Note added succesfully", Toast.LENGTH_SHORT).show();
  }

  @Override public void onNoteUpdated(int id) {
    Toast.makeText(this, "Note with: " + String.valueOf(id) + " updated succesfully!", Toast.LENGTH_SHORT).show();
  }

  @Override public void displayTitleInvalid() {
    tlNoteTitle.setErrorEnabled(true);
    tlNoteTitle.setError(getString(R.string.please_insert_note_title));
  }

  @Override public void displayBodyInvalid() {
    tlNoteBody.setErrorEnabled(true);
    tlNoteBody.setError(getString(R.string.please_insert_note_body));
  }

  @Override public void finishInserting() {
    finish();
  }
}
