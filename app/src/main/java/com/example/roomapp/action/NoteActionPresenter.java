package com.example.roomapp.action;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.example.roomapp.AppDatabase;
import com.example.roomapp.note.Note;
import org.w3c.dom.Text;

public class NoteActionPresenter {
  private AppDatabase appDatabase;
  private View view;

  public NoteActionPresenter(Context context) {
    appDatabase = AppDatabase.getInstance(context);
  }

  public void setView(View view) {
    this.view = view;
  }

  public void saveNote(String title, String body) {
    Handler handler = new Handler(Looper.getMainLooper());

    if(TextUtils.isEmpty(title)) {
      view.displayTitleInvalid();
      return;
    }

    if(TextUtils.isEmpty(body)) {
      view.displayBodyInvalid();
      return;
    }

    new Thread(new Runnable() {
      @Override
      public void run() {
        appDatabase.noteDao().insertNote(new Note(title, body));
        handler.post(new Runnable() {
          @Override
          public void run() {
            view.finishInserting();
            view.onNoteAdded();
          }
        });
      }
    }).start();
  }

  public void updateNote(Note note) {
    Handler handler = new Handler(Looper.getMainLooper());
    new Thread(new Runnable() {
      @Override
      public void run() {
        appDatabase.noteDao().updateNote(note);
        handler.post(new Runnable() {
          @Override
          public void run() {
            view.finishInserting();
            view.onNoteUpdated(note.getId());
          }
        });
      }
    }).start();
    
  }

  public interface View {
    void onNoteAdded();
    void displayTitleInvalid();
    void displayBodyInvalid();
    void finishInserting();

    void onNoteUpdated(int id);
  }
}
