package com.example.roomapp;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;
import com.example.roomapp.note.Note;
import com.example.roomapp.note.NoteWithTags;
import com.example.roomapp.tag.Tag;
import java.util.List;

@Dao
public interface NoteDao {

  @Query("select  * from note")
  List<Note> getNoteList();

  @Query("select * from note where id = :id")
  Note getNoteById(int id);
  
  @Insert
  void insertNote(Note note);

  @Update
  void updateNote(Note note);

  @Delete
  void deleteNote(Note note);

  @Transaction
  @Query("select * from note")
  List<NoteWithTags> getNoteWithTags();

  @Insert
  void insertTag(Tag tag);
}
