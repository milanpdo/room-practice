package com.example.roomapp;

import android.content.DialogInterface;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.example.roomapp.action.NoteActionActivity;
import com.example.roomapp.note.Note;
import com.example.roomapp.note.NoteAdapter;
import com.example.roomapp.note.NoteDetailsActivity;
import com.example.roomapp.note.NotePresenter;
import com.facebook.stetho.Stetho;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.List;

public class MainActivity extends AppCompatActivity
    implements NotePresenter.View, NoteAdapter.NoteAdapterListener {
  private NoteAdapter noteAdapter;
  @BindView(R.id.activityMainRv) RecyclerView recyclerView;
  private NotePresenter presenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Stetho.initializeWithDefaults(this);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    presenter = new NotePresenter(this);
    presenter.setView(this);
    presenter.getAllNotes();
  }

  @Override protected void onResume() {
    super.onResume();
    presenter.getAllNotes();
  }

  @Override protected void onPause() {
    super.onPause();
    presenter.getAllNotes();
  }

  @OnClick(R.id.activityMainFab)
  public void startActionActivity() {
    Intent intent = new Intent(getApplicationContext(), NoteActionActivity.class);
    startActivity(intent);
  }

  @Override public void displayNotes(List<Note> notes) {
    noteAdapter = new NoteAdapter(this, notes);
    noteAdapter.setNoteAdapterListener(this);
    recyclerView.setAdapter(noteAdapter);
    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    recyclerView.setHasFixedSize(true);
  }

  @Override public void displayEmptyNotes() {
    Toast.makeText(this, "EMPTY NOTES", Toast.LENGTH_SHORT).show();
  }

  @Override public void onNoteClick(int id) {
    Intent intent = new Intent(getApplicationContext(), NoteDetailsActivity.class);
    intent.putExtra(Constants.NOTE_ID, id);
    startActivity(intent);
  }

  @Override public void onNoteLongClick(Note note) {
    presenter.deleteNote(note);
  }

  @Override public void onDeleteNote(String noteTitle) {
    Toast.makeText(this, "Deleted note: "  + noteTitle, Toast.LENGTH_SHORT).show();

  }
}
