package com.example.roomapp.note;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import com.example.roomapp.R;
import java.util.ArrayList;
import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {

  private Context context;
  private List<Note> noteList;
  private Note note;
  private NoteAdapterListener noteAdapterListener;

  public NoteAdapter(Context context, List<Note> noteList) {
    this.context = context;
    this.noteList = noteList;
  }

  public void setNoteAdapterListener(NoteAdapterListener noteAdapterListener) {
    this.noteAdapterListener = noteAdapterListener;
  }

  @NonNull @Override
  public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item, parent, false);
    NoteViewHolder viewHolder = new NoteViewHolder(v);
    return viewHolder;
  }

  @Override public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
    note = noteList.get(position);
    holder.itemNoteTvId.setText(String.valueOf(note.getId()));
    holder.itemNoteTvTitle.setText(note.getTitle());
    holder.itemNoteTvBody.setText(note.getBody());
  }

  @Override public int getItemCount() {
    return noteList.size();
  }

  public class NoteViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.itemNoteTvId) TextView itemNoteTvId;
    @BindView(R.id.itemNoteTvTitle) TextView itemNoteTvTitle;
    @BindView(R.id.itemNoteTvBody) TextView itemNoteTvBody;


    public NoteViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    @OnClick(R.id.itemNoteRlParent)
    public void onNoteClick() {
      noteAdapterListener.onNoteClick(noteList.get(getAdapterPosition()).getId());
    }

    @OnLongClick(R.id.itemNoteRlParent)
    public void onNoteLongClick() {
      noteAdapterListener.onNoteLongClick(noteList.get(getAdapterPosition()));
    }
  }

  public interface NoteAdapterListener{
    void onNoteClick(int id);
    void onNoteLongClick(Note note);
  }

}
