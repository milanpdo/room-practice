package com.example.roomapp.note;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.example.roomapp.AppDatabase;

public class NoteDetailsPresenter {
  private AppDatabase appDatabase;
  private View view;
  private Note note;

  public NoteDetailsPresenter(Context context) {
    this.appDatabase = AppDatabase.getInstance(context);
  }

  public void setView(View view) {
    this.view = view;
  }

  public void getNoteById(int id) {
    Handler handler = new Handler(Looper.getMainLooper());
    new Thread(new Runnable() {
      @Override
      public void run() {
        note = appDatabase.noteDao().getNoteById(id);
        handler.post(new Runnable() {
          @Override
          public void run() {
            view.displayNote(note);
          }
        });
      }
    }).start();
  }

  public interface View {
    void displayNote(Note note);
  }
}
