package com.example.roomapp.note;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "note")
public class Note {

  @PrimaryKey(autoGenerate = true)
  private int id;
  @ColumnInfo(name = "title")
  private String title;
  @ColumnInfo(name = "body")
  private String body;


  public Note(String title, String body) {
    this.title = title;
    this.body = body;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }
}
