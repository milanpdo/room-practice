package com.example.roomapp.note;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.example.roomapp.AppDatabase;
import java.util.ArrayList;
import java.util.List;

public class NotePresenter {
  private AppDatabase appDatabase;
  private View view;
  private List<Note> notes;

  public NotePresenter(Context context) {
    this.appDatabase = AppDatabase.getInstance(context);

  }

  public void setView(View view) {
    this.view = view;
  }

  public void getAllNotes() {
    Handler handler = new Handler(Looper.getMainLooper());
    new Thread(new Runnable() {
      @Override
      public void run() {
        notes = appDatabase.noteDao().getNoteList();
        handler.post(new Runnable() {
          @Override
          public void run() {
            view.displayNotes(notes);
          }
        });
      }
    }).start();
  }

  public void deleteNote(Note note) {
    Handler handler = new Handler(Looper.getMainLooper());
    new Thread(new Runnable() {
      @Override
      public void run() {
        appDatabase.noteDao().deleteNote(note);
        handler.post(new Runnable() {
          @Override
          public void run() {
            view.onDeleteNote(note.getTitle());
          }
        });
      }
    }).start();
  }

  public interface View {
    void displayNotes(List<Note> notes);
    void displayEmptyNotes();
    void onDeleteNote(String noteTitle);
  }
}
