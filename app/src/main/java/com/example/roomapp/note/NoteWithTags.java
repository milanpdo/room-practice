package com.example.roomapp.note;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Junction;
import androidx.room.Relation;
import com.example.roomapp.NoteTagCrossRef;
import com.example.roomapp.tag.Tag;
import java.util.List;

public class NoteWithTags {
  @Embedded private Note note;
  @Relation(
      parentColumn = "id",
      entityColumn = "tagId"
  )
  private List<Tag> tags;

  public Note getNote() {
    return note;
  }

  public void setNote(Note note) {
    this.note = note;
  }

  public List<Tag> getTags() {
    return tags;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }
}
