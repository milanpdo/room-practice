package com.example.roomapp.note;

import android.widget.TextView;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.example.roomapp.Constants;
import com.example.roomapp.R;
import com.example.roomapp.action.NoteActionActivity;
import com.example.roomapp.tag.AddTagActivity;
import com.google.gson.Gson;

public class NoteDetailsActivity extends AppCompatActivity implements NoteDetailsPresenter.View {

  @BindView(R.id.activityNoteDetailsDisplayedId) TextView activityNoteDetailsTvId;
  @BindView(R.id.activityNoteDetailsDisplayedBody) TextView activityNoteDetailsTvBody;
  @BindView(R.id.activityNoteDetailsDisplayedTitle) TextView activityNoteDetailsTvTitle;
  private NoteDetailsPresenter presenter;
  private Note note;
  private int noteId;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_note_details);
    ButterKnife.bind(this);

    presenter = new NoteDetailsPresenter(this);
    presenter.setView(this);

    if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.NOTE_ID)) {
      noteId = getIntent().getExtras().getInt(Constants.NOTE_ID);
      presenter.getNoteById(noteId);
    }
  }

  @Override protected void onResume() {
    super.onResume();
    presenter.getNoteById(noteId);
  }

  @Override protected void onPause() {
    super.onPause();
    presenter.getNoteById(noteId);
  }

  @Override public void displayNote(Note note) {
    this.note = note;
    activityNoteDetailsTvId.setText(String.valueOf(note.getId()));
    activityNoteDetailsTvTitle.setText(note.getTitle());
    activityNoteDetailsTvBody.setText(note.getBody());
  }

  @OnClick(R.id.activityNoteDetailsBtnEdit)
  public void onBtnEditClick() {
    Intent intent = new Intent(getApplicationContext(), NoteActionActivity.class);
    intent.putExtra(Constants.IS_EDIT, true);
    String noteJson = new Gson().toJson(this.note);
    intent.putExtra(Constants.NOTE, noteJson);
    startActivity(intent);
  }

  @OnClick(R.id.activityNoteDetailsBtnAddTag)
  public void onBtnAddTagClick() {
    Intent intent = new Intent(getApplicationContext(), AddTagActivity.class);
    intent.putExtra(Constants.NOTE_ID, noteId);
    startActivity(intent);
  }
}
