package com.example.roomapp;

import androidx.room.Entity;

@Entity(primaryKeys ={"id", "tagId"})
public class NoteTagCrossRef {
  private int id;
  private int tagId;
}
