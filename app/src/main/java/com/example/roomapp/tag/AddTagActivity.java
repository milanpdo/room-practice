package com.example.roomapp.tag;

import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.example.roomapp.Constants;
import com.example.roomapp.R;
import com.google.android.material.textfield.TextInputLayout;

public class AddTagActivity extends AppCompatActivity implements AddTagPresenter.View {

  @BindView(R.id.activityAddTagTlName) TextInputLayout activityAddTagTlName;
  private int noteId;
  private AddTagPresenter presenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_tag);
    ButterKnife.bind(this);

    presenter = new AddTagPresenter(this);
    presenter.setView(this);

    if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.NOTE_ID)) {
      noteId = getIntent().getExtras().getInt(Constants.NOTE_ID);
    }

  }

  @OnClick(R.id.activityAddTagBtnSave)
  public void insertNewTag() {
    presenter.saveTag(activityAddTagTlName.getEditText().getText().toString(), noteId);
  }


  @Override public void displayNameInvalid() {
    activityAddTagTlName.setErrorEnabled(true);
    activityAddTagTlName.setError(getString(R.string.please_insert_Tag_name));
  }

  @Override public void finishInserting() {
    finish();
  }

  @Override public void onTagAdded() {
    Toast.makeText(this, "Tag succesfully added", Toast.LENGTH_SHORT).show();
  }
}
