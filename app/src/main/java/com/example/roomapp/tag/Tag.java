package com.example.roomapp.tag;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tag")
public class Tag {
  @PrimaryKey(autoGenerate = true)
  private int tagId;
  @ColumnInfo(name = "name")
  private String name;
  @ColumnInfo(name ="noteId")
  private int noteId;

  public Tag(String name, int noteId) {
    this.name = name;
    this.noteId = noteId;
  }


  public int getTagId() {
    return tagId;
  }

  public void setTagId(int tagId) {
    this.tagId = tagId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getNoteId() {
    return noteId;
  }

  public void setNoteId(int noteId) {
    this.noteId = noteId;
  }
}
