package com.example.roomapp.tag;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.example.roomapp.AppDatabase;
import com.example.roomapp.note.Note;

public class AddTagPresenter {
  private AppDatabase appDatabase;
  private View view;
  
  public AddTagPresenter(Context context) {
    appDatabase = AppDatabase.getInstance(context);
  }

  public void setView(View view) {
    this.view = view;
  }
  
  public void saveTag(String name, int id) {
    Handler handler = new Handler(Looper.getMainLooper());
    if(TextUtils.isEmpty(name)) {
      view.displayNameInvalid();
      return;
    }

    new Thread(new Runnable() {
      @Override
      public void run() {
        appDatabase.noteDao().insertTag(new Tag(name, id));
        handler.post(new Runnable() {
          @Override
          public void run() {
            view.finishInserting();
            view.onTagAdded();
          }
        });
      }
    }).start();

  }

  public interface View {

    void displayNameInvalid();

    void finishInserting();

    void onTagAdded();
  }
}
