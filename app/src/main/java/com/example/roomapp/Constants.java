package com.example.roomapp;

public class Constants {
  public static final String NOTE_ID = "note_id";
  public static final String IS_EDIT = "is_edit";
  public static final String NOTE = "note";
}
